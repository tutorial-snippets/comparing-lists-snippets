package main

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"sync"
	"time"
	"github.com/adam-hanna/arrayOperations" //I'll use arrayOperations just to make it easier
)

const pathListA string = "../csv/list_a.csv"
const pathListB string = "../csv/list_b.csv"

type InsertionList struct{
	mu  sync.Mutex
	List map[string]int
}


func (i *InsertionList)Initialize(){
	i.List = make(map[string]int)
}

func (i *InsertionList)insert(name string, score int){
	i.mu.Lock()
	v, e := i.List[name]
	if e {
		i.List[name] = v + score
		i.mu.Unlock()
		return
	}
	i.List[name] = score
	i.mu.Unlock()
}

func (i *InsertionList)InsertFromA(name string){
	i.insert(name, 1)
}

func (i *InsertionList)InsertFromB(name string){
	i.insert(name, 2)
}

func (i *InsertionList)CollectResult(ins, outs, reps *[]string){
	for key, value := range i.List {
		//value 1 means it only exists on list A
		if value == 1 {
			*outs = append(*outs, key)
			continue
		}
		//even values means it only exists on list B
		if value%2 == 0{
			*ins = append(*ins, key)
			continue
		}
		//odd values means it exists on both lists
		if value%2 == 1{
			*reps = append(*reps, key)
			continue
		}
	}
}

//reads the file line by line
func readFileToList(filePath string, onReadItem func(name string) ){
	csvfile, err := os.Open(filePath)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	r := csv.NewReader(csvfile)
	first_line := true
	for {	
		if first_line {
			first_line = false
			continue
		}	
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		onReadItem(record[0])
	}
}
//for async call
func readListA(wg *sync.WaitGroup, i *InsertionList){
	readFileToList(pathListA, i.InsertFromA)
	wg.Done()
}
//for async call
func readListB(wg *sync.WaitGroup, i *InsertionList){
	readFileToList(pathListB, i.InsertFromB)
	wg.Done()
}

func comparingOnInsertion(){
	var i InsertionList
	i.Initialize()

	var wg sync.WaitGroup

	var readLists = []func(*sync.WaitGroup, *InsertionList){ 
		readListA, readListB,
	} 
	
	wg.Add(len(readLists))
	for _, task := range readLists { 
        go task(&wg, &i) 
    } 
	wg.Wait()
	
	var ins, outs, reps []string
	i.CollectResult(&ins, &outs, &reps)

	log.Println("Ins: ", len(ins),  " | Outs: " , len(outs), " | Reps: ", len(reps))
}

func loadList(filePath string, list *[]string){
	readFileToList(filePath, func(name string){
		*list = append(*list, name)
	})
}

func loadListA(wg *sync.WaitGroup, result *[]string){
	loadList(pathListA, result)
	wg.Done()
}

func loadListB(wg *sync.WaitGroup, result *[]string){
 	loadList(pathListB, result)
	wg.Done()
}

func comaring() {

	var listA, listB []string
	var wg sync.WaitGroup
	wg.Add(2)
	go loadListA(&wg, &listA) 
	go loadListB(&wg, &listB) 
    
	wg.Wait()	

	reps, _ := arrayOperations.Intersect(listA, listB)
	diff, _ := arrayOperations.Difference(listA, listB)
	ins, _ := arrayOperations.Intersect(listB, diff.Interface().([]string))
	outs, _ := arrayOperations.Intersect(listA, diff.Interface().([]string))

	log.Println("Ins: ", len(ins.Interface().([]string)),  " | Outs: " , len(outs.Interface().([]string)), " | Reps: ", len(reps.Interface().([]string)))

}

func main(){

	start_1 := time.Now()
	comparingOnInsertion()
	elapsed_1 := time.Since(start_1)
	log.Printf("compareOnInsertion took %s", elapsed_1)
	
	start_2 := time.Now()
	comaring()
	elapsed_2 := time.Since(start_2)
	log.Printf("normal compare took %s", elapsed_2)

}