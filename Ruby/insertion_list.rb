require 'csv'

class InsertionList
    def initialize()
        @list = {}
        #@mutex = Mutex.new
    end

    def insert(name, score)
        #@mutex.lock
        @list[name].nil? ? @list[name] = score : @list[name] += score 
        #@mutex.unlock
    end

    def insert_a(name)
        self.insert(name,1)
    end
    
    def insert_b(name)
        self.insert(name,2)
    end

    def collect
        ins = []
        outs = []
        reps = []

        @list.each do |k,v|
            if v==1
                outs << k
                next
            end
            v%2==0 ? ins << k : reps << k
        end
        {
            ins: ins, 
            outs: outs,
            reps: reps
        }
    end
end

def read_csv(path_to_csv)
    names = []
    csv_contents = CSV.read(path_to_csv)
    csv_contents.shift
    csv_contents.each do |row|
        names << row[0]
    end
    names
end

def read_list_a(path_to_csv, insertion_list)
    csv_contents = CSV.read(path_to_csv)
    csv_contents.shift
    csv_contents.each do |row|
        insertion_list.insert_a(row[0])
    end
    true
end

def read_list_b(path_to_csv, insertion_list)
    csv_contents = CSV.read(path_to_csv)
    csv_contents.shift
    csv_contents.each do |row|
        insertion_list.insert_b(row[0])
    end
    true
end

#==============================
pathListA = "../csv/list_a.csv"
pathListB = "../csv/list_b.csv"

start = Time.now
list_a = []
list_b = []
thread_c = Thread.new do
    list_a = read_csv(pathListA)
end

thread_d = Thread.new do
    list_b = read_csv(pathListB)
end

if thread_c.join && thread_d.join
    r = {
        ins:  (list_b - list_a).count,
        outs: (list_a - list_b).count,
        reps: list_a.intersection(list_b).count,
    }
    puts r
    puts "Comparing took #{Time.now - start}" 
end

GC.start# just to make sure there was no cache before we start

insertion_list = InsertionList.new
start = Time.now
thread_a = Thread.new do
    read_list_a(pathListA, insertion_list)
end

thread_b = Thread.new do
    read_list_b(pathListB, insertion_list)
end

if thread_a.join && thread_b.join
    result = insertion_list.collect
    r = {
        ins: result[:ins].count,
        outs: result[:outs].count,
        reps: result[:reps].count,
    }
    puts r
    puts "compareOnInsertion took #{Time.now - start}" 
end



